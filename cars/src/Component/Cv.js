import React, { Component } from 'react'

import logo  from  './logo.png';
import Page from './Page'


 class Cv extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
             numero:'77-105-96-90',
             email:'baaida1998@gmail.com',
             adress:'Liberté 4 ',
             comp:'competence et technique',
             loisire:'loisir'

         }
     }
    
    
    render() {
        const titre={
            fontSize:'300%',
            fontWeight:'800',
            textTransform:'uppercase',
            textAlign:'center',
            marginTop:'2%',
            marginLeft:'5%',
            color:'white'
        }
        const i={
            position:'absolue',
            left:'6%',
            top:'70%'
        }
        const borde={
            
           
            textTransform:'uppercase',
            marginTop:'2%',
            marginLeft:'8%',
            
            color:'white'

        }
        const style={
            fontSize:'10px',
            color:'white'
        }
       
        const{tech1,tech2,tech3,tech4,name}=this.props;
        return (
            <div>
            <div className='container'>
                <div className='profil'>
                <img src={logo} alt=""/>
                <h5 style={titre}>{name}</h5>
                </div>
                <div className='ligne'  style={{borderBottom:'2px solid cornflowerblue',marginTop:'-5%',width:'70%',marginLeft:'30%'}}>
                <i className='lig' class="fas fa-phone-alt"></i> <p>{this.state.numero}</p>
                    <p style={style}>mobile</p>
                    

                </div>
               <div className='ligne'  style={{borderBottom:'2px solid cornflowerblue',marginTop:'10%',width:'70%',marginLeft:'30%'}}>
                <i class="fas fa-envelope" style={i}></i><p>{this.state.email}</p>
                <p style={style}>personnel</p>
                    
 
                </div>
                <div className='ligne' className='icon' style={{borderBottom:'2px solid cornflowerblue',marginTop:'15%',width:'70%',marginLeft:'30%'}}>
                <i class="fas fa-map-marker-alt"  ></i> <p>{this.state.adress}</p>
                  <p style={style}>adresse</p>
                    

                </div>
                <div>
               
                <div className='progress'  className='conp' style={{borderBottom:'2px solid cornflowerblue',marginTop:'15%',width:'70%',marginLeft:'30%'}}>
                <i class="fas fa-briefcase"></i><h5  style={{marginLeft:'-8%',color:'white',fontWeight:'bold',textTransform:'uppercase',fontSizw:'100%'}}>{this.state.comp}</h5>
                   
                   <div className='bord'></div>
                 <p  style={borde}> {tech1}</p>
                 
                <div className='progress'  style={{borderBottom:'2px solid cornflowerblue',marginTop:'15%',width:'100%',marginLeft:'0px'}}></div>
                 <div className='bord1' style={{borderRadius:'85%',width:'10px',height:'10px',left:'20%',top:'142.7%',position:'absolute',marginLeft:'150px',backgroundColor:'white'}}></div>
                 <p style={borde}> {tech2}</p>
                 
                <div className='progress'  style={{borderBottom:'2px solid cornflowerblue',marginTop:'15%',width:'100%',marginLeft:'0px'}}></div>
                 <div className='bord2' style={{borderRadius:'85%',width:'10px',height:'10px',left:'20%',top:'155.5%',position:'absolute',marginLeft:'160px',backgroundColor:'white'}}></div>
                 <p style={borde}> {tech3}</p>
                 
                <div className='progress'  style={{borderBottom:'2px solid cornflowerblue',marginTop:'15%',width:'100%',marginLeft:'0px'}}></div>
                 <div className='bord3' style={{borderRadius:'85%',width:'10px',height:'10px',left:'20%',top:'163.1%',position:'absolute',marginLeft:'150px',backgroundColor:'white'}}></div>
                 <p style={borde}> {tech4}</p>
                
                </div>
                
            </div>
            </div>
            <div  style={{marginTop:'-25%'}} className='loi'>
            <i class="fas fa-list"></i> <h5  style={{marginLeft:'10%',color:'white',fontWeight:'bold',textTransform:'uppercase',fontSize:'100%'}} >{this.state.loisire}</h5>
               <div></div>
               <p style={borde}>sport</p>
                 <div ></div>
               <p style={borde}>lecture</p>
                    
 
                </div>
            
            <Page year='2020' year1='2021'year2='2020-2021' year3='2019-2020'year4='2018-2019'year5='2017-2018'/>

                
            </div>
        )
    }
}

export default Cv
