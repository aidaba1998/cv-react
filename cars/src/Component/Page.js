import React, { Component } from 'react'

 class Page extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
              text1:'qui suis -je ?',
              text2:'parcours experience',
              text3:'parcours de formation'
         }
     }
     
     
    render() {
       
        return (
            <div>
            <div className='modal'>
            <i class="fas fa-user"></i>  <h5 style={{fontWeight:'3000',marginTop:'-50px',textAlign:'center',fontSize:'200%',textTransform:'uppercase'}}>{this.state.text1}</h5>
             <p style={{fontSize:'100%',lineHeight:'1.75',textAlign:'center'}}>je me nomme Aida Ba étudiante  à l institut polythechnique de Dakar et developpeuse web .J ai 21 ans et je suis diplomé d un BTS <br/>et licence3 à l institut polythechnique de Dakar  </p>
            </div>
            <div className='modal1'>
            <i class="fas fa-briefcase"></i> <h5 style={{fontWeight:'3000',marginTop:'-40px',textAlign:'center',fontSize:'200%',textTransform:'uppercase'}}>{this.state.text2}</h5>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}> Gerante de boutique</p>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year1} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}> Consommatrice des tutoriels de developpement web</p>
             
            </div>
            <div className='modal2'>
            <i class="fas fa-graduation-cap"></i> <h5 style={{fontWeight:'3000',marginTop:'-50px',marginLeft:'12%',fontSize:'200%',textTransform:'uppercase'}}>{this.state.text3}</h5>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year2} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}> Licence3,Génie logiciel et administration réseaux en à l IPD</p>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year3} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}> BTS en informatique de gestion à l institut polytechnique de Dakar</p>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year4} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}> Licence 1 en informatique de gestion</p>
             <h3 style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>{this.props.year5} </h3>
             <p style={{fontSize:'100%',marginLeft:'10%',fontWeight:'200%'}}>Bacalaureat au lycée Ababacar Sy de Tivaouane</p>
             
            </div>
                
            </div>
        )
    }
}

export default Page
